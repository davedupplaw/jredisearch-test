import io.redisearch.Query
import io.redisearch.Schema
import io.redisearch.client.Client
import java.io.File
import io.redisearch.SearchResult
import kotlin.system.measureTimeMillis


class JRediSearchKotlin(val articles: List<Article>) {
    fun index(): Int {
        val client = getClient()

        val sc = Schema().addTextField("title", 2.0)
                         .addTextField("body", 1.0)

        client.createIndex( sc, Client.IndexOptions.Default() )

        articles.forEach { a ->
            client.addDocument( a.id, mapOf( "title" to a.title, "body" to a.body ) )
        }

        return articles.size
    }

    fun query( query: String ): SearchResult? = getClient().search( Query(query).limit(0, 5 ) )
    fun drop() = getClient().dropIndex();

    private fun getClient() = Client("reuters", "localhost", 6379)
}

fun main(args: Array<String>) {
    val articles = ReutersLoader().load( File("./data/reut2-000.sgm") )

    val jrs = JRediSearchKotlin( articles )
    var numIndexed = 0;
    var time = measureTimeMillis { numIndexed = jrs.index(); }
    println( "Indexed ${numIndexed} items in ${time} milliseconds" );

    var searchResult: SearchResult? = null;
    time = measureTimeMillis { searchResult = jrs.query("bilat") }
    println( searchResult!!.docs );
    println( "Took ${time} milliseconds" );

    jrs.drop();
}