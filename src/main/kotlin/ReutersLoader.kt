import java.io.File

data class Article( val id: String, val title: String, val body: String )

class ReutersLoader {
    fun load( file: File) : List<Article> {
        val articles = mutableListOf<Article>()
        val lines = file.readLines()

        val titlePtn = Regex(".*<TITLE>([^<]*)</TITLE>.*")
        val bodyPtn  = Regex(".*<BODY>([^<]*)</BODY>.*")
        val idPtn    = Regex( """.*NEWID="([0-9]+)".*""")

        var currentTitle: String? = null
        var currentBody: String? = null
        var currentId : String? = null
        var currentLine = ""
        var lineCount = 0
        lines.forEach { l ->
            lineCount++
            currentLine += " "+l

            if( idPtn.matches(currentLine) ) {
                currentId = idPtn.find(currentLine)?.groupValues?.get(1)
                currentLine = "";
            }

            if( titlePtn.matches(currentLine) ) {
                currentTitle = titlePtn.find(currentLine)?.groupValues?.get(1)?.replace("&lt;","<")
                currentLine = ""
            }

            if( bodyPtn.matches(currentLine) ) {
                currentBody = bodyPtn.find(currentLine)?.groupValues?.get(1)?.replace("&lt;","<")
                currentLine = ""
            }

            if( currentTitle != null && currentBody != null ) {
                articles.add( Article( currentId!!, currentTitle!!, currentBody!! ) )
                currentLine = ""
                currentTitle = null
                currentBody = null
            }
        }
        return articles
    }
}